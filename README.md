# imp

The imp programming language. 


# Example

**main.imp**

```

// This is a comment

/*
 * This is also a comment.
 * Functions are denoted by starting with a capital letter.
 * Any program you 'run' will need a Main function.
 */

Main(_args@str[], _argCount@i32)
{
    /*
     * Main is passed two arguments. One is an array of arguments.
     * The other is the number of arguments.
     */

    /*
     * Variables start with an underscore. You can denote the type
     * with an '@' symbol or the type can be infered.
     * You reference them without the '_'
     * All statments must end with ';' or a compound statement '{}'
     */
    _foo@i32 = 10;
    _bar = 10;

    // Array
    _myArray@i32 = [3;10,23,1]

    //Vector
    _myArray@i32 = [*]

    //HashMap
    _myArray@i32 = [&]

    /* 
     * Variables can only contain numbers or letters
     */
    _thisIsValid = true;
    _this_is_not_valid = false;
    _this-is-also-not-valid = false;

    ?(foo == bar) 
    {
        // This is an if statment, they do not use the keyword if. Instead
        // they start with '?'
    }:?{
        //This is an else if
    }:{
        // else is denoted by a ':'
    }

    #()
    {
        // This is an infinite while loop
        // unless you break with 
        ##;
    } 

    #(foo > 1)
    {
        // This loop will eventually break.
        foo -= 1;
    }
    

    #(_i=0;i<foo;i++)
    {
        //For loop
    }

    // This is a function
    // Arguments must be given a type
    // There must be a return type if the
    // function returns.
    // '>;' will return that value
    AddTwo(_input@i32) -> i32
    {  
        _sum = input + 2;
        sum >;
    }

    //This is how you call a function
    _newValue = AddTwo(10);
    
    // Finally, you can organize variables/functions in a 
    // namespace as follows.

    CoolVars
    {
        _coolLevel@i32 = 10;
        HowCool() -> i32{
           coolLevel >; 
        }
    }

    // There are no objects, only static types.
    CoolVars::coolLevel;
    CoolVars::HowCool();
}
```
